import "package:flutter_test/flutter_test.dart";

import "package:static_sharedpreferences/static_sharedpreferences.dart" as sp;

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() async {
    await sp.initialize();
  });

  const Map<String, Object> kTestValues = {
    "flutter.bool": true,
    "flutter.double": 3.14159,
    "flutter.int": 42,
    "flutter.List": <String>["foo", "bar"],
    "flutter.String": "hello world",
  };

  test("writing", () async {
    await Future.wait([
      sp.setBool("flutter.bool", kTestValues["flutter.bool"] as bool),
      sp.setDouble("flutter.double", kTestValues["flutter.double"] as double),
      sp.setInt("flutter.int", kTestValues["flutter.int"] as int),
      sp.setStringList(
          "flutter.List", kTestValues["flutter.List"] as List<String>),
      sp.setString("flutter.String", kTestValues["flutter.String"] as String),
    ]);
  });

  test("reading", () {
    expect(sp.getAny("flutter.bool"), kTestValues["flutter.bool"]);
    expect(sp.getAny("flutter.double"), kTestValues["flutter.double"]);
    expect(sp.getAny("flutter.int"), kTestValues["flutter.int"]);
    expect(sp.getAny("flutter.List"), kTestValues["flutter.List"]);
    expect(sp.getAny("flutter.String"), kTestValues["flutter.String"]);
    expect(sp.getBool("flutter.bool"), kTestValues["flutter.bool"]);
    expect(sp.getDouble("flutter.double"), kTestValues["flutter.double"]);
    expect(sp.getInt("flutter.int"), kTestValues["flutter.int"]);
    expect(sp.getStringList("flutter.List"), kTestValues["flutter.List"]);
    expect(sp.getString("flutter.String"), kTestValues["flutter.String"]);
  });
}
