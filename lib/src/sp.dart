import 'package:shared_preferences/shared_preferences.dart';

late SharedPreferences _instance;

/// ## Description
///
/// * You `have to initialize static_sharedpreferences` before you can use
///   any of the methods contained in this library.
///
///
/// * Loads and parses the [SharedPreferences] for this app from disk.
///   Because this is reading from disk, it shouldn't be awaited in
///   performance-sensitive blocks.
///
///
/// ## Example
///
/// {@tool sample}
/// ```dart
/// import 'package:static_sharedpreferences/static_sharedpreferences.dart' as sp;
///
/// void main() {
///   WidgetsFlutterBinding.ensureInitialized();
///
///   sp.initialize().whenComplete((){
///     print('Done with initialization!');
///   });
///   ...
/// }
/// ```
/// {@end-tool}
Future<void> initialize() async {
  _instance = await SharedPreferences.getInstance();
}

/// ## Description
///
/// * Execute some code if this is the first time the user
///   has launched your app.
///
///
/// ## Parameters
///
/// * [key] - The key used for retrieving the information
///   if the user has or has not launched this app yet.
///
///
/// * [onFirstTimeLaunched] - Execute some code when the
///   `user launches this app for the first time.`
///
///
/// * [onNormalLaunch] - Execute some code if the
///   `user has already` launched your app in the past.
bool onFirstTimeLaunched({
  required String key,
  void Function()? onFirstTimeLaunched,
  void Function()? onNormalLaunch,
}) {
  if (getBool(key) ?? false) {
    onFirstTimeLaunched?.call();
    return true;
  } else {
    onNormalLaunch?.call();
  }

  return false;
}

/// Completes with true once the user preferences for the app has been cleared.
Future<bool> clear() => _instance.clear();

/// Returns true if persistent storage the contains the given [key].
bool containsKey(String key) => _instance.containsKey(key);

/// Removes an entry from persistent storage.
Future<bool> remove(String key) => _instance.remove(key);

/// Fetches the latest values from the host platform.
Future<void> reload() => _instance.reload();

/// Returns all keys in the persistent storage.
Set<String> getKeys() => _instance.getKeys();

Object? getAny(String key) => _instance.get(key);

bool? getBool(String key) => _instance.getBool(key);

double? getDouble(String key) => _instance.getDouble(key);

int? getInt(String key) => _instance.getInt(key);

String? getString(String key) => _instance.getString(key);

List<String>? getStringList(String key) => _instance.getStringList(key);

Future<bool> setBool(String key, bool value) => _instance.setBool(key, value);

Future<bool> setDouble(String key, double value) =>
    _instance.setDouble(key, value);

Future<bool> setInt(String key, int value) => _instance.setInt(key, value);

Future<bool> setString(String key, String value) =>
    _instance.setString(key, value);

Future<bool> setStringList(String key, List<String> value) =>
    _instance.setStringList(key, value);
