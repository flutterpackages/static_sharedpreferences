# static_sharedpreferences

Package for reading and writing simple key-value pairs. This wraps the [shared_preferences](https://pub.dev/packages/shared_preferences) plugin from the Flutter Team.

## Getting Started

You have to call `initialize()` before using any methods from this package.
After the initialization process you are good to go to use any getters, setters
or whatever method you need.

```dart
  import "package:static_sharedpreferences/static_sharedpreferences.dart" as sp;

  void main() {
    WidgetsFlutterBinding.ensureInitialized();

    sp.initialize().whenComplete((){
      print('Done with initialization!');
    });
  
    bool? test = sp.getBool("testBoolKey");
  }
```
