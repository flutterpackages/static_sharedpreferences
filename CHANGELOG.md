## 2.0.1

- Updated README.md

## 2.0.0

- Rewrote entire implementation of SP class.
- Update constraints for shared_preferences plugin from the Flutter Team.
- Added some test cases.

## 1.4.0

- Added listenable broadcast streams for every value type setter and getter

## 1.3.5

- Fixed a bug in [onFirstTimeLaunched]

## 1.3.4

- Included a value for [firstTimeLaunchedKey]

## 1.3.3

- Fixed a Bug where you could not initialize SP if you depended on a package which also
  uses SP.

## 1.3.2

- changed shared_preferences: 0.5.7+3 to shared_preferences: ^0.5.7+3

## 1.3.1

- Updated dependencies

## 1.3.0

- Added onFirstTimeLaunched Function

## 1.2.1

- Changed onSharedPreferencesIsNull and a few docs a bit.

## 1.2.0

- Fixed bugs. This is a stable version.

## 1.1.4

- Fixed execute method.

## 1.1.3

- Added execute method.

## 1.0.3

- Added more debug info.

## 1.0.2

- Added a static bool to check if SP is has successfully initialized.

## 1.0.1

- Changed onSharedPreferencesIsNull default value.

## 1.0.0

- First release.
